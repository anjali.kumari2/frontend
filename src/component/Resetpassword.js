import React from 'react';
class Resetpassword extends React.Component {
    constructor() {
    super();
    this.state = {
      input: {},
      errors: {}
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange(event) {
    let input = this.state.input;
    input[event.target.name] = event.target.value;
    this.setState({
      input
    });
  }
  handleSubmit(event) {
    event.preventDefault();
    if(this.validate()){
        console.log(this.state);
        let input = {};
        input["password"] = "";
        input["confirm_password"] = "";
        this.setState({input:input});
        alert('Resetpassword is submited');
    }
  }
  validate(){
      let input = this.state.input;
      let errors = {};
      let isValid = true;
      if (!input["password"]) {
        isValid = false;
        errors["password"] = "Please enter your password.";
      }
      if (!input["confirm_password"]) {
        isValid = false;
        errors["confirm_password"] = "Please enter your confirm password.";
      }
      if (typeof input["password"] !== "undefined" && typeof input["confirm_password"] !== "undefined") {
        if (input["password"] !== input["confirm_password"]) {
          isValid = false;
          errors["password"] = "Passwords don't match.";
        }
      }
      this.setState({
        errors: errors
      });
      return isValid;
  }
   render() {
    return (
       <div>
       <h1>password reset of sapas telematics system pvt ltd</h1>
        <form onSubmit={this.handleSubmit}>
       <div class="form-group">
            <label for="password">Password:</label>
            <input
              type="password"
              name="password"
              value={this.state.input.password}
              onChange={this.handleChange}
              class="form-control"
              placeholder="Enter password"
              id="password" />
              <div className="text-danger">{this.state.errors.password}</div>
          </div>
          <div class="form-group">
            <label for="password">Confirm Password:</label>
            <input
              type="password"
              name="confirm_password"
              value={this.state.input.confirm_password}
              onChange={this.handleChange}
              class="form-control"
              placeholder="Enter confirm password"
              id="confirm_password" />
              <div className="text-danger">{this.state.errors.confirm_password}</div>
          </div>
          <input type="submit" value="Submit" class="btn btn-success"/>
        </form>
      </div>
    );
  }
}
export default Resetpassword;