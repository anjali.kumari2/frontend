import React, { Component } from "react";
import { Map, Marker, Popup, Tooltip, Polyline } from "react-leaflet";
import L from "leaflet";
import '../LeafletCluster.css';
import MarkerClusterGroup from "react-leaflet-markercluster";
import ReactLeafletGoogleLayer from 'react-leaflet-google-layer';
import axios from 'axios';

export default class LeafletCluster extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lat:18.559008,
      lng:-68.388881,
      zoom: 15,
      maxZoom: 30,
      path : [],
      progress: [],
      counter: 0,
      paths : []
    };
  }
  
  pushCoordsToPath = (res) => {
    var data = res.data;
    var path=[];
    for (var i=0; i< data.length; i++)
    {
      var multi=[];
      for (var j =0; j < data[i].position.length; j++) {
        multi.push({"lat": parseFloat(data[i].position[j].latitude), "lng": parseFloat(data[i].position[j].longitude)});
      }
      path.push(multi)
    }
    this.setState({
      path:path,
      paths:data
    });
  }

  componentDidMount = () => {
    axios.get('http://localhost:8000/LiveMap/coordinates/')
    .then(res => {this.pushCoordsToPath(res)})
    .catch(err => {console.log(err)})
    
    this.interval = window.setInterval(this.moveObject, 10000);
  };

  
  componentWillUnmount = () => {
    window.clearInterval(this.interval);
  };

  moveObject = () => {
    var currentPoints=[];
    for (var i=0; i< this.state.path.length; i++)
    {
      var myArr = []
      for (var j=0; j<=this.state.counter; j++) {
        if (j >= this.state.path[i].length) {
          break;
        }
        console.log("in path", this.state.path[i][j])
        myArr.push(this.state.path[i][j])
      }
      currentPoints.push(myArr)
      // if(this.state.counter < this.state.path[i].length)
      // {
      //   currentPoints.push(this.state.path[i][this.state.counter])
      // }
      // else{
      //   currentPoints.push(this.state.path[i][this.state.path[i].length-1])
      // }
    }
    const progress = currentPoints;
    this.setState({ counter:this.state.counter+1 });
    // if (this.state.counter >= this.state.path.length) {
    //   return;
    // }
    console.log("before setting progress",progress);
    this.setState({ progress });
    console.log("after setting progress",progress);
  }

  customIconCreateFunction(cluster) {
    return L.divIcon({
      html: `<span>${cluster.getChildCount()}</span>`,
      className: "marker-cluster-custom",
      iconSize: L.point(40, 40, true)
    });
  }

   renderPopup(index) {
     if (index >= this.state.paths.length) {
       return
     }

     return (
       <Popup
         tipSize={5}
         anchor="bottom-right"
         longitude={this.state.paths[index].position[0].longitude}
         latitude={this.state.paths[index].position[0].latitude}
         onMouseLeave={() => this.setState({ popupInfo: null })}
         closeOnClick={true}>
          rootClose={false}
        
         <p>
           <strong>Vehicle Number: {this.state.paths[index].vehicle_number}</strong><br/>
           <strong>Speed: {this.state.paths[index].speed}</strong><br/>
           <strong>Location: {this.state.paths[index].current_location}</strong>
         </p>
       </Popup>
     );
   }
  
  render() {
    if (!this.state.progress) {
      return <span>loading...</span>
    }

    const position = [this.state.lat, this.state.lng];
    return (
      <Map center={position} zoom={4} maxZoom={20}>
        <ReactLeafletGoogleLayer googleMapsLoaderConf={{KEY: 'AIzaSyDZyk30qeWjK_Ba8d08upoqsj9wkfJ-L1g'}}/>
        <MarkerClusterGroup
          showCoverageOnHover={false}
          spiderfyDistanceMultiplier={2}
          iconCreateFunction={this.customIconCreateFunction}>
          {this.state.progress.length>0 && this.state.progress.map((marker, index) => {
            console.log(index)
            return (
              <div>
                <Polyline key={index} positions={this.state.progress} color={'red'} />
                  <Marker zoom={4} key={index} position={marker[marker.length-1]}>
                  {this.renderPopup(index)}
                </Marker>
              </div>
            );
          })}
        </MarkerClusterGroup>
      </Map>);
  }
}

