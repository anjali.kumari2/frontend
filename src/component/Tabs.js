import React from 'react';
import '../Tabs.css'

function Tabs() {
  return (
    <div className="tabs">
  <div class="nav">
    <div class="col">
      <div class="current" >
        <img src = {process.env.PUBLIC_URL + '/Elements/LiveTracking_hover.svg' } alt="Live Map" className="icon" /> Live Map
      </div>
    </div>
    <div class="col">
      <div class="bg-sec ">
        <img src = {process.env.PUBLIC_URL + '/Elements/Report.svg' } alt="Report" className="icon" /> Reports
      </div>
    </div>
     <div class="col">
      <div class="bg-sec ">
        <img src = {process.env.PUBLIC_URL + '/Elements/Playback.svg' } alt="Playback" className="icon" />Playback
      </div>
    </div>
    <div class="col">
      <div class="bg-sec ">
       <img src = {process.env.PUBLIC_URL + '/Elements/Geofencing.svg' } alt="Geofencing" className="icon" />Geo Fencing
      </div>
    </div>
    <div class="col">
      <div class="bg-sec ">
      <img src = {process.env.PUBLIC_URL + '/Elements/TourPlanning.svg' } alt="TourPlanning" className="icon" />Tour Planning
      </div>
    </div>
    <div class="col">
      <div class="bg-sec ">
     <img src = {process.env.PUBLIC_URL + '/Elements/Dashboard.svg' } alt="Dashboard" className="icon" />Dashboard
      </div>
    </div>
    <div class="col">
      <div class="bg-sec ">
      <img src = {process.env.PUBLIC_URL + '/Elements/Maintenance.svg' } alt="Maintenance" className="icon" />Maintenance
      </div>
    </div>
    <div class="col">
      <div class="bg-sec ">
      <img src = {process.env.PUBLIC_URL + '/Elements/Temperature.svg' } alt="Temperature" className="icon" />Temperature
      </div>
    </div>
    <div class="col">
      <div class="bg-sec ">
      <img src = {process.env.PUBLIC_URL + '/Elements/Alerts.svg' } alt="Alerts" className="icon" />Alerts
      </div>
    </div>
    <div class="col">
      <div class="bg-sec ">
      <img src = {process.env.PUBLIC_URL + '/Elements/Settings.svg' } alt="Settings" className="icon"  />Settings
      </div>
    </div>
</div>
</div>
  );
}
export default Tabs;