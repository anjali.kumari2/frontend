import React from 'react';
import '../Footer.css'

function Footer() {
  return (
   <div className=" footer">
  <div class="row">

            <div className="col-md-3">
                <div className="bg-sec1 ">
                     <img src = {process.env.PUBLIC_URL + '/Elements/Vehicle Status.svg' } alt="VehicleStatus" className="icon" /> Vehicle Status
                </div>

                 <div className="bg-sec1 ">
                      <img src = {process.env.PUBLIC_URL + '/Elements/Driver Performance.svg' } alt="Driver Performance" className="icon" /> Driver Performance
                  </div>

                  <div className="bg-sec1 ">
                        <img src = {process.env.PUBLIC_URL + '/Elements/Journey History.svg' } alt="Journey History" className="icon" /> Journey History
                  </div>

            </div>

            <div className="col-md-9 footercontent" >
                <h6 style={{'line-height':'10'}}>No Records Available</h6>
            </div>

  </div>
</div>
  );
}
export default Footer;