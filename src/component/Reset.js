import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import {faEye, faEyeSlash} from '@fortawesome/free-solid-svg-icons';
import LoginService from '../service/PostData'

const loginService = new LoginService();
class Reset extends React.Component {
    constructor(props) {
    super(props);
    this.state = {
      input: {},
      errors: {}
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange(event) {
   const { password, confirm_password } = this.state;
    const matches = password === confirm_password;
     {
          this.handleUpdate();
        }
  event.preventDefault();
    matches ? alert("password match") : alert("NO MATCH");
  }
  handleSubmit(event) {
    event.preventDefault();
     console.log('welcome to react')
     if(this.validatePassword() && this.validateEmail()){
        let input = {};
        input["npwd"] = "";
        input["cpwd"] = "";
        this.setState({input:input});
    }
    loginService.login(
         {
          "newpwd": this.state.input.npwd,
          "cnfmpwd":this.state.input.cpwd ,
      }
      )
      .then((result)=>{
        console.log(result)
       window.location.href="./"
      })
      .catch(()=>{
         alert('please enter correct email and password');
      });
  }
  validatePassword(){
      let input = this.state.input;
      let errors = {};
      let isValid = true;
      if (!input["npwd"]) {
        isValid = false;
        errors["npwd"] = "Please enter your Password.";
      }
     if (typeof input["npwd"] !== "undefined") {
      var pattern = new RegExp(/^(?=.*\d)(?=.*[a-zA-Z])[a-zA-Z0-9]{7,}$/);
      if (!pattern.test(input["npwd"])) {
          isValid = false;
          errors["npwd"] = "Please enter valid password.";
        }
     }
     this.setState({
        errors: errors
      });
      return isValid;
  }



  state = {
    isPasswordShown: false
  };
  togglePasswordVisiblity = () => {
    const { isPasswordShown } = this.state;
    this.setState({ isPasswordShown: !isPasswordShown });
    }
   render()
   { const { isPasswordShown } = this.state;
      return(
        <div>< img src = {process.env.PUBLIC_URL + '/background.png' } alt="Background" className="bg" />
            <div  className="center" ><center><img src = {process.env.PUBLIC_URL + '/logo.svg' } alt="Logo" className="logo" /><br/></center>
                 <div>
                    <form>
                        <div class="form-group"> <label for="email"></label>
                        <input type="text" name="newpwd" value={this.state.input.npwd}  onChange={this.handleChange} class="form-control" placeholder="Enter New Password"
                        id="email" />
                        <div className="text-danger">{this.state.errors.email}</div>
                        </div>
                        <div class="form-group">
                        <label for="pwd"></label>
                        <input type={isPasswordShown ? "text" : "password"} name="cnfmpwd" value={this.state.input.cpwd}  onChange={this.handleChange} class="form-control" placeholder="Enter Confirm Password"
                        id="pwd"/>
                        <div className="eye" >

					   <FontAwesomeIcon icon={isPasswordShown ? faEyeSlash : faEye}  onClick={this.togglePasswordVisiblity}/>
					   </div>
                        <div className="text-danger">{this.state.errors.pwd}</div>
                        </div>
                            <input type="button" value="Login" onClick={this.handleSubmit} class="btn btn-primary" />


                    </form>
        </div>
        </div>
        </div>
        );
        }
        }
export default Reset;