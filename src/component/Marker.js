import React, { useState, useEffect } from 'react';
import Map from './component/Map';

// API key of the google map
const GOOGLE_MAP_API_KEY = '<AIzaSyDZyk30qeWjK_Ba8d08upoqsj9wkfJ-L1g&libraries=geometry,drawing,places>';

// load google map script
const loadGoogleMapScript = (callback) => {
  if (typeof window.google === 'object' && typeof window.google.maps === 'object') {
    callback();
  } else {
    const googleMapScript = document.createElement("script");
    googleMapScript.src = `https://maps.googleapis.com/maps/api/js?key=AIzaSyDZyk30qeWjK_Ba8d08upoqsj9wkfJ-L1g&libraries=geometry,drawing,places`;
    window.document.body.appendChild(googleMapScript);
    googleMapScript.addEventListener("load", callback);
  }
}

const App = () => {
  const [loadMap, setLoadMap] = useState(false);

  useEffect(() => {
    loadGoogleMapScript(() => {
      setLoadMap(true)
    });
  }, []);

  return (
    <div className="App">
      <a href="https://www.cluemediator.com">Clue Mediator</a><br /><br />
      {!loadMap ? <div>Loading...</div> : <Map />}
      <br/><br/>
      <small><b>Note:</b> In order to make it work, you have to set the https://maps.googleapis.com/maps/api/js?key=AIzaSyDZyk30qeWjK_Ba8d08upoqsj9wkfJ-L1g&libraries=geometry,drawing,places in App.js file. </small>
    </div>
  );
}

export default App