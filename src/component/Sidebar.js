import React, {Component} from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faCar,  faThumbtack,faTruck,faMotorcycle} from '@fortawesome/free-solid-svg-icons';
import '../Sidebar.css';
import axios from 'axios';
class Sidebar extends Component {
 constructor(props) {
    super(props);
    this.state = {
      paths: [],
      path : []
    };
  }
   componentDidMount() {
        let data ;
       axios.get('http://localhost:8000/LiveMap/coordinates/')
            .then(res => {
            console.log( "res : ",res)
            data = res.data;
             console.log("initial data : ",data)
            this.setState({
                paths : data
            });
                       console.log("state paths : ",this.state.paths)
        })
        .catch(err => {})
    }
render()
{
return(


            <div className=" v-list ">

                <form>
                     <input className="side-search" placeholder="Search for..."  />
                </form>


                <div className="breadcum">
                    <div className="">
                        <a href="">SAPAS ></a>
                        <i className="fa fa-angle-double-right" aria-hidden="true"></i>
                        <a href="">Eastzone ></a>
                        <i className="fa fa-angle-double-right" aria-hidden="true"></i>
                        <a href="">Eastwestern</a>
                     </div>
                </div>




                        {this.state.paths.map((path,index) => (
                        <div className="row seperator">

                                <div className="col-lg-1 col-md-1 col-sm-1 " >
                                    <FontAwesomeIcon icon={faMotorcycle} size='1x' color='#005383'/>
                                </div>
                                <div className="col-lg-5 col-md-3 col-sm-3">
                                    <h6>{path.vehicle_number}</h6>
                                </div>
                                <div className="col-lg-1 col-md-1 col-sm-1">
                                   <FontAwesomeIcon icon={faThumbtack} size='1x' color='#005383'/>
                                </div>
                                <div className="col-lg-4 col-md-3 col-sm-3 text text-right ">
                                    <p>{path.speed}</p>
                                </div>
                        </div>
                         ))}

                </div>




);
}
}
export default Sidebar;