import React, { useState, Fragment } from "react";
import {
  useLoadScript,
  GoogleMap,
  Marker,
  InfoWindow
} from "@react-google-maps/api";
import useSwr from "swr";


const fetcher = (...args) => fetch(...args).then(response => response.json());
export default function LocationTracking() {
  const [ setMapRef] = useState(null);
  const [selectedPlace, setSelectedPlace] = useState(null);
  const [markerMap, setMarkerMap] = useState({});
  const [center, setCenter] = useState({ lat: 22.286580, lng: 73.168691 });
  const [zoom, setZoom] = useState(5);
  const [clickedLatLng, setClickedLatLng] = useState(null);
  const [infoOpen, setInfoOpen] = useState(false);
   const url =
    'http://localhost:8000/locationTracking/coordinates/';
  const { data, error } = useSwr(url, { fetcher });
  const paths = data && !error ? data : [];


  const { isLoaded } = useLoadScript({
    googleMapsApiKey: "AIzaSyDZyk30qeWjK_Ba8d08upoqsj9wkfJ-L1g"
  });


  const fitBounds = map => {
    const bounds = new window.google.maps.LatLngBounds();
    paths.map(path => {
      bounds.extend(path.latitude,path.longitude);
      return path.id;
    });
    map.fitBounds(bounds);
  };


  const loadHandler = map => {
    setMapRef(map);
    fitBounds(map);
  };


  const markerLoadHandler = (marker, path) => {
    return setMarkerMap(prevState => {
      return { ...prevState, [path.id]: marker };
    });
  };


  const markerClickHandler = (event, place) => {
    setSelectedPlace(place);
    if (infoOpen) {
      setInfoOpen(false);
    }
    setInfoOpen(true);
    if (zoom < 13) {
      setZoom(13);
    }
    setCenter(place.pos)
  };


  const renderMap = () => {
    return (
      <Fragment>
        <GoogleMap
          onLoad={loadHandler}
        //  onCenterChanged={() => setCenter(mapRef.getCenter().toJSON())}
          onClick={e => setClickedLatLng(e.latLng.toJSON())}
          center={center}
          zoom={zoom}
          mapContainerStyle={{
            height: "70vh",
            width: "100%"
          }}
        >
          {paths.map(path => (
            <Marker
              key={path.id}
              position={{ lat: Number.parseFloat(path.latitude), lng: Number.parseFloat(path.longitude) }}
              onLoad={marker => markerLoadHandler(marker, path)}
              onClick={event => markerClickHandler(event, path)}
            />
          ))}
          {infoOpen && selectedPlace && (
            <InfoWindow
              anchor={markerMap[selectedPlace.id]}
              onCloseClick={() => setInfoOpen(false)}
            >
              <div>
                <h3>{selectedPlace.id}</h3>
                <div>This is your info window content</div>
              </div>
            </InfoWindow>
          )}
        </GoogleMap>

        <h3>
          Center {center.lat}, {center.lng}
        </h3>

        {clickedLatLng && (
          <h3>
            You clicked: {clickedLatLng.lat}, {clickedLatLng.lng}
          </h3>
        )}

        {selectedPlace && <h3>Selected Marker: {selectedPlace.id}</h3>}
      </Fragment>
    );
  };
  return isLoaded ? renderMap() : null;
}