import React, {Component} from 'react';
import '../Header.css';
class Header extends Component {
render()
{
return(
<div class="header-top">
<div className="container-fluid">
    <div class="row">
       <div className="col-lg-2">
            <img src = {process.env.PUBLIC_URL + '/logoblue.svg' } alt="sapas" className="headerlogo " />

            SAPAS Telematics
       </div>
        <div className="col-lg-3">
            <form >
                <div className="">
                <input className="header-search" placeholder="Search "   />
                </div>
            </form>
        </div>

        <div className="col-lg-3">
            <div class="row">

                <div className="outerblock">
                    <div className="innerblock">
                       <div style={{'color':'#008923'}} className="summarycounter"> 000 </div>
                       <div style={{'color':'#008923'}} className="summarycount"> Moving </div>
                    </div>
                </div>
                <div className="outerblock">
                    <div className="innerblock">
                       <div style={{'color':'#d20000'}} className="summarycounter"> 000 </div>
                       <div style={{'color':'#d20000'}} className="summarycount"> Idle </div>
                    </div>
                </div>
                <div className="outerblock">
                    <div className="innerblock">
                       <div style={{'color':'#d67b23'}} className="summarycounter"> 000 </div>
                       <div style={{'color':'#d67b23'}} className="summarycount"> Parked </div>
                    </div>
                </div> <div style={{'margin':'20px 20px 20px 50px'}} >Welcome Priya Tripathi</div>


            </div>
        </div>
                <div style={{'padding-left':'310px'}} className="col-lg-4  " >
                    <img src = {process.env.PUBLIC_URL + '/Elements/Fullscreen.svg' } alt="Fullscreen" className="controls " />
                    <img src = {process.env.PUBLIC_URL + '/Elements/Alertnotify.svg' } alt="Alertnotify" className="controls " />
                    <img src = {process.env.PUBLIC_URL + '/Elements/Notifications.svg' } alt="Notifications" className="controls " />
                    <img src = {process.env.PUBLIC_URL + '/Elements/User.svg' } alt="User" className="controls " />
                    <img src = {process.env.PUBLIC_URL + '/Elements/Logout.svg' } alt="Logout" className="controls " />
                </div>
     </div>
</div>
</div>
  );
}
}
export default Header;