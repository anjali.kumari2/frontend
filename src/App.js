
/*import logo from './logo.svg';*/
import './App.css';
import Login from './component/Login'
import Forget from './component/Forget'
import Reset from './component/Reset'
import Resetpassword from './component/Resetpassword'
import Hello from './component/Hello'
import Acknowledgement from './component/Acknowledgement'
import Header from './component/Header'
import Tabs from './component/Tabs'
import Sidebar from './component/Sidebar'
import Footer from './component/Footer'
import Map from './component/Map'
import Screen from './component/Screen'
import Testmap from './component/Testmap'
import {Route, Switch, Redirect} from "react-router-dom";
import { withCookies} from 'react-cookie';
import React  from 'react';
//const ProtectedRoute = ({ component: Comp, loggedIn, path, ...rest }) => {
//  return (
//    <Route
//      path={path}
//      {...rest}
//      render={(props) => {
//        return loggedIn ? (
//          <Comp {...props} {...rest} />
//        ) : (
//          <Redirect
//            to={{
//              pathname: "/",
//              state: {
//                prevLocation: path,
//                error: "You need to login first!",
//              },
//            }}
//          />
//        );
//      }}
//    />
//  );
//};

//const authentication={
//  isLoggedIn:false,
//  onAuthentication(){
//    this.isLoggedIn=true;
//  },
//  getLogInStatus(){
//    return this.isLoggedIn;
//  }
//}
//function SecuredRoute(props){
//  return(
//    <Route path={props.path} render={data=>authentication.getLogInStatus()?(
//      <props.component {...data}></props.component>):
//      (<Redirect to={{pathname:'/'}}></Redirect>)}></Route>
//  )
//}


function App() {
  return (
    <div className="App">
    <Switch>
       <Route path='/' exact component={Login}/>
       <Route path='/forget' component={Forget}/>
       <Route path='/Reset' component={Reset}/>
       <Route  path='/hello' exact component={Hello}/>
       <Route path='/acknowledgement' exact component={Acknowledgement}/>
       <Route path='/header' exact component={Header}/>
       <Route path='/tabs' exact component={Tabs}/>
       <Route path='/sidebar' exact component={Sidebar}/>
       <Route path='/footer' exact component={Footer}/>
       <Route path='/resetpassword' exact component={Resetpassword}/>
       <Route path='/screen' exact component={Screen}/>
       <Route path='/map' exact component={Map}/>
       <Route path='/testmap' exact component={Testmap}/>
    </Switch>
      <br/><br/>
    </div>
  );
}




export default withCookies(App);